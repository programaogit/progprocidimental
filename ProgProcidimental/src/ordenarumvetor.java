import java.util.Scanner;


public class ordenarumvetor {
	private static Scanner entrada;
	public static void main(String[] args) {

		entrada = new Scanner(System.in);
		System.out.println("");
		System.out.println("Programa que tenha uma fun��o para ordenar um vetor, retornando esse vetor ordenado");
		System.out.println("");
		
		System.out.println("Inserir numero de casas?  ");
		int size = entrada.nextInt();
		double[] array = new double[size] ;
		
		for (int i = 0 ; i < array.length; i++) {
			System.out.println("");
			System.out.println("Inserir numero na posi�ao "+i);
			System.out.println("");
			array[i] = entrada.nextDouble();
			
		}

		ordernar(array);
		for (int i = 0; i < array.length; i++) {
			System.out.println("");
			System.out.println(array[i]);
		}
	}
	
	public static void ordernar(double[] array) {
		
		for (int i = 0 ; i <= array.length-1; i++) {
			for (int j = i+1; j < array.length; j++) {
				if (array[j] < array[i]) {
					double temp = array[j];
					array[j] = array[i];
					array[i] = temp;
				}
			}
		}
		
	}

}