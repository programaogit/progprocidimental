import java.util.Scanner;
public class Maioralga {
	private static Scanner entrada;
	public static void main(String[] args) {
		
		System.out.println("");
		System.out.println("Programa que tenha uma fun��o que recebe um n�mero inteiro e devolve o maior algarismo contido nesse n�mero");
		System.out.println("");
		
		entrada = new Scanner(System.in);
		System.out.println("Introduza o n�mero: ");
		int numero = entrada.nextInt();
		System.out.println("");
		System.out.println("O maior algarismo contido no n�mero �: "+ amaior(numero));
		
	}
	public static int amaior(int numero) {
		int max = numero % 10 ;
		
		while (numero != 0) {
			int alg = numero % 10;
			numero = (numero - alg) / 10 ;
			if (alg > max) {
				max = alg;
			}
		}
		int maior = max;
		return maior;
	}

}