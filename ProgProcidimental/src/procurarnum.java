import java.util.Scanner;
public class procurarnum {
	private static Scanner entrada;

	static boolean procuravetor(double[] array)
	{
		System.out.println("Qual � o numero ?");
		double procurar = entrada.nextDouble();

		for (int i = 0; i < array.length;i++) {
			if (array[i] == procurar) {
				System.out.println("O numero esta na posi�ao "+ i);
				return true;
				
			}
		}
		return false;
	}

	public static void main(String[] args) {

		entrada = new Scanner(System.in);

		System.out.println("Implemente um programa que tenha uma fun��o para procurar um numero num vetor, retornando �true� caso detete o n�mero, �false� caso contr�rio");
		System.out.println("");
		System.out.println("Numero de posi�oes?");

		int tamanho= entrada.nextInt();
		double[] array = new double[tamanho] ;

		for (int i = 0 ; i < array.length; i++) {
			System.out.println("Inserir numero na posi�ao "+i);
			array[i] = entrada.nextDouble();
		}
		
		procuravetor(array);
	}
}