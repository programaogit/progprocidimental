import java.util.Scanner;
public class potenica {
	private static Scanner entrada;
	public static double power(double numero, double expoente) {
		
		double result = Math.pow(numero, expoente);
		return result;
	}
	
	public static void main(String[] args) {
		
		System.out.println("");
		System.out.println("Programa para calcular a potencia");
		System.out.println("");
		
		entrada = new Scanner(System.in);
		System.out.println("Numero da potencia: ");
		double numero = entrada.nextDouble();
		System.out.println("Expoente da potencia: ");
		double expoente = entrada.nextDouble();
		System.out.println("");
		System.out.println(numero+" ^ "+expoente+" = "+power(numero, expoente));
		
	}
	
}