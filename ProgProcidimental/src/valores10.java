import java.util.Scanner;

public class valores10 {
	
		///////////////////////////////////////////////////////////////////////////////////////////
	
private static Scanner entrada;
	public static void Vetor() {
		System.out.println("");
		System.out.println("Uma fun��o que fa�a a leitura de 10 valores, guardando-os num vetor");
		System.out.println("");
		
		int[] numeros = new int[10];
		entrada = new Scanner(System.in);
		for (int i = 0; i < 10; i++) {
			System.out.println("Introduza o " + i + "�: ");
			numeros[i] = entrada.nextInt();
		}	
		
		////////////////////////////////////////////////////////////////////////////////////////////////////
		
		System.out.println("O maior n�mero �: "+maximo(numeros));
		System.out.println("O menor n�mero �: "+minimo(numeros));
		
		System.out.println("A diferen�a entre o maior e menor �: "+diferenca(maximo(numeros), minimo(numeros)));
		System.out.println("Tem "+pares(numeros)+" n�meros pares e "+impares(numeros, pares(numeros))+" n�meros impares.");
	}
	
	
		///////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static double maximo(int[] array) {
		double max = Double.NEGATIVE_INFINITY;
		for (int i=0; i<10; i++) {
			if (array[i] > max) {
				max = array[i];
			}
		}
		return max;
	}
	
	public static double minimo(int[] array) {
		double min = Double.POSITIVE_INFINITY;
		for (int i=0; i<10; i++) {
			if (array[i] < min) {
				min = array[i];
			}
		}	
		return min;
	}
	
	public static double diferenca(double max, double min) {
		double resultado = max - min ;
		return resultado;
	}

	public static int pares(int[] array) {
		int pares = 0;
		for (int i = 1; i<10; i++) {
			if (array[i] % 2 == 0) {
				pares++;
			}
		}
		return pares;
	}
	
	public static int impares(int[] array, int pares) {
		int impares = array.length - pares;
		return impares;
	}
	
	public static void main(String[] args) {
		
		
		Vetor();
		
		
	}

}